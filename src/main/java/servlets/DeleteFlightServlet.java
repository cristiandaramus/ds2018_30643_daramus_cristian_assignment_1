package servlets;

import service.FlightService;
import service.UserService;
import util.SessionData;
import util.Util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class DeleteFlightServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        response.setContentType("text/html");

        UserService userService = SessionData.userService;
        PrintWriter out = response.getWriter();


        if (userService.findUserRoleByUsername(SessionData.username).toLowerCase().equals("admin")) {

            String url = request.getRequestURL().toString();
            Integer flightNumber = Integer.valueOf(url.substring(url.indexOf("delete") + 7, url.length()));

            FlightService flightService = SessionData.flightService;
            Boolean deleted = flightService.delete(flightNumber);

            if (deleted) {
                response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
                response.setHeader("Location", "http://localhost:8080/admin");
            } else {
                out.println(Util.getServerError());
            }
        } else {
            out.println(Util.getNotAuthorizedHtml());
        }
    }
}
