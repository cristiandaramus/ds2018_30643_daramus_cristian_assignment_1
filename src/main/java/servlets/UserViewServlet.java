package servlets;

import model.Flight;
import service.FlightService;
import service.UserService;
import util.SessionData;
import util.Util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class UserViewServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        UserService userService = SessionData.userService;
        FlightService flightService = SessionData.flightService;

        PrintWriter out = response.getWriter();

        String html = Util.getNotAuthorizedHtml();

        if (userService.findUserRoleByUsername(SessionData.username).toLowerCase().equals("user")) {
            List<Flight> flights = flightService.findFlights();
            html = getHtml(flights);
        }
        out.println(html);
    }

    private String getHtml(List<Flight> flights) {

        StringBuilder html = new StringBuilder("<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">\n");
        html.append("<html>\n")
                .append("<head></head>\n")
                .append("<body bgcolor = \"#f0f0f0\">\n")
                .append("<ul>\n");

        for (Flight flight : flights) {
            html.append("<li><b>FlightNr: ")
                    .append(flight.getId())
                    .append("  Origin:")
                    .append(flight.getDepartureCity())
                    .append("  Destination:")
                    .append(flight.getArrivalCity())
                    .append("  Departure Date:")
                    .append(flight.getDepartureDateTime())
                    .append("</b>")
                    .append("<a href=\"localhost:8080/flight/")
                    .append(flight.getId())
                    .append("\">")
                    .append("   Click for info")
                    .append("</a>")
                    .append("\n");
            html.append("</li>");
        }

        html.append("</ul>\n")
                .append("</body>")
                .append("</html>");

        return html.toString();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
