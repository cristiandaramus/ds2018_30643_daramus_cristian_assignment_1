package servlets;

import model.Flight;
import service.FlightService;
import service.UserService;
import util.SessionData;
import util.Util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.URL;

public class FlightViewServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.setContentType("text/html");

        String url = request.getRequestURL().toString();
        Integer flightNumber = Integer.valueOf(url.substring(url.indexOf("flight") + 7, url.length()));
        FlightService flightService = SessionData.flightService;
        UserService userService = SessionData.userService;
        Flight flight = flightService.findFlightById(flightNumber);

        PrintWriter out = response.getWriter();

        String userRole = userService.findUserRoleByUsername(SessionData.username).toLowerCase();
        if (userRole.equals("admin") || userRole.equals("user")) {
            String html = getHtml(flight);

            out.println(html);
        } else {
            out.println(Util.getNotAuthorizedHtml());
        }

    }


    private String getHtml(Flight flight) {
        StringBuilder html = new StringBuilder("<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">\n");
        html.append("<html>\n")
                .append("<head></head>\n")
                .append("<body bgcolor = \"#f0f0f0\">\n");
        for (Field field : Flight.class.getDeclaredFields()) {
            field.setAccessible(true);
            String fieldName = field.getName().toUpperCase();
            try {
                Object filedValue = field.get(flight);
                if (fieldName.toLowerCase().contains("citylocaltime")) {
                    String lat = filedValue.toString().substring(0, filedValue.toString().indexOf("&"));
                    String lng = filedValue.toString().substring(filedValue.toString().indexOf("&") + 1, filedValue.toString().length());
                    filedValue = getLocalTime(lng, lat);
                }
                html.append("<div>")
                        .append(fieldName)
                        .append(": ")
                        .append(filedValue)
                        .append("</div>");
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        html
                .append("</body>")
                .append("</html>");

        return html.toString();
    }

    private String getLocalTime(String lng, String lat) {
        StringBuilder apiURL = new StringBuilder("http://api.timezonedb.com/v2.1/get-time-zone?key=GJQ49YXOJK66&format=xml&by=position&lat=")
                .append(lat)
                .append("&lng=")
                .append(lng);

        try {
            URL obj = new URL(apiURL.toString());
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            String response = new String();
            while ((inputLine = in.readLine()) != null) {
                if (inputLine.contains("<formatted>"))
                    response = inputLine
                            .substring(inputLine.indexOf("<formatted>") + 11, inputLine.indexOf("</formatted>"));
            }
            in.close();
            return response;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "";
    }

}
