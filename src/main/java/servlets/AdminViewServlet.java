package servlets;

import model.Flight;
import service.FlightService;
import service.UserService;
import util.SessionData;
import util.Util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class AdminViewServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");

        UserService userService = SessionData.userService;
        FlightService flightService = SessionData.flightService;

        PrintWriter out = response.getWriter();

        if (userService.findUserRoleByUsername(SessionData.username).toLowerCase().equals("admin")) {
            out.println(getHtml(flightService.findFlights()));
        } else {
            out.println(Util.getNotAuthorizedHtml());
        }

    }

    private String getHtml(List<Flight> flights) {
        StringBuilder html = new StringBuilder("<!doctype html public \"-//w3c//dtd html 4.0 transitional//en\">\n");
        html.append("<html>\n")
                .append("<head></head>\n")
                .append("<body bgcolor = \"#f0f0f0\">\n")
                .append("<div>")
                .append("<a href=\"localhost:8080/flight/create\">")
                .append("Create")
                .append("</a>\n")
                .append("</div>\n")
                .append("<div>\n")
                .append("<ul>\n");

        for (Flight flight : flights) {
            html.append("<li><b>FlightNr</b>: ")
                    .append("<a href=\"localhost:8080/flight/")
                    .append(flight.getId())
                    .append("\">")
                    .append(flight.getId())
                    .append("</a>")
                    .append("<a href=\"localhost:8080/flight/update/")
                    .append(flight.getId())
                    .append("\"> update</a>")
                    .append("<a href=\"localhost:8080/flight/delete/")
                    .append(flight.getId())
                    .append("\"> delete</a>")
                    .append("\n");
        }

        html.append("</ul>\n")
                .append("</div>\n")
                .append("</body>")
                .append("</html>");

        return html.toString();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
