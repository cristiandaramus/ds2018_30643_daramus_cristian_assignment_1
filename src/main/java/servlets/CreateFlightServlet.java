package servlets;

import model.Flight;
import service.FlightService;
import service.UserService;
import util.SessionData;
import util.Util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Timestamp;

public class CreateFlightServlet extends HttpServlet {


    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserService userService = SessionData.userService;
        PrintWriter out = response.getWriter();

        if (userService.findUserRoleByUsername(SessionData.username).toLowerCase().equals("admin")) {
            out.println(Util.getFlightForm("create"));

        } else {
            out.println(Util.getNotAuthorizedHtml());
        }
    }

    // Method to handle POST method request.
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String airplaneType = request.getParameter("airplaneType");
        String departureCity = request.getParameter("departureCity");
        StringBuilder departureDateTimeString = new StringBuilder(request.getParameter("departureDateTime"))
                .append(":00");
        departureDateTimeString.setCharAt(10, ' ');
        Timestamp departureDateTime = Timestamp.valueOf(departureDateTimeString.toString());
        String departureCityLocalTime = request.getParameter("departureCityLocalCoord");
        String arrivalCity = request.getParameter("arrivalCity");
        StringBuilder arrivalDateTimeString = new StringBuilder(request.getParameter("arrivalDateTime"))
                .append(":00");
        arrivalDateTimeString.setCharAt(10, ' ');
        Timestamp arrivalDateTime = Timestamp.valueOf(arrivalDateTimeString.toString());
        String arrivalCityLocalTime = request.getParameter("arrivalCityCoord");

        FlightService flightService = SessionData.flightService;

        Flight flight = new Flight(airplaneType, departureCity, departureDateTime, departureCityLocalTime, arrivalCity, arrivalDateTime, arrivalCityLocalTime);

        Flight save = flightService.save(flight);

        if (save.getId() == null) {
            PrintWriter out = response.getWriter();
            out.println(Util.getServerError());
        } else {
            response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", "http://localhost:8080/admin");
        }
    }
}
