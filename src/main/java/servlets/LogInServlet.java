package servlets;

import exceptions.LoginException;
import service.UserService;
import util.SessionData;
import util.Util;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class LogInServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        UserService userService = SessionData.userService;

        try {

            String userRole = userService.logIn(request.getParameter("username"), request.getParameter("password"));

            response.setStatus(HttpServletResponse.SC_MOVED_TEMPORARILY);
            response.setHeader("Location", "http://localhost:8080/" + userRole.toLowerCase());

        } catch (LoginException e) {
            PrintWriter out = response.getWriter();
            out.println(Util.getNotAuthorizedHtml());
        }
    }

    // Method to handle POST method request.
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        doGet(request, response);
    }
}

