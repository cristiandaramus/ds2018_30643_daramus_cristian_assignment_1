package util;

public class Util {

    public static String getNotAuthorizedHtml() {
        return "<html>\n" +
                "<header><title>No</title></header>\n" +
                "<body>\n" +
                "Authorization Fail\n" +
                "</body>\n" +
                "</html>";
    }

    public static String getServerError() {
        return "<html>\n" +
                "<header><title>No</title></header>\n" +
                "<body>\n" +
                "Server Error\n" +
                "</body>\n" +
                "</html>";
    }

    public static String getFlightForm(String action) {
        return new StringBuilder("<html>\n")
                .append("<body>\n")
                .append("<form action =")
                .append("\"")
                .append(action)
                .append("\"")
                .append(" method = \"POST\">\n")
                .append("Airplane Type: <input type = \"text\" name = \"airplaneType\">\n")
                .append("<br />\n")
                .append("departureCity: <input type = \"text\" name = \"departureCity\" />\n")
                .append("<br />\n")
                .append("departureDateTime: <input type = \"datetime-local\" name = \"departureDateTime\" />\n")
                .append("<br />\n")
                .append("departureCityLocalCoord: <input type = \"text\" name = \"departureCityLocalCoord\" />\n")
                .append("<br />\n")
                .append("arrivalCity: <input type = \"text\" name = \"arrivalCity\" />\n")
                .append("<br />\n")
                .append("arrivalDateTime: <input type = \"datetime-local\" name = \"arrivalDateTime\" />\n")
                .append("<br />\n")
                .append("arrivalCityCoord: <input type = \"text\" name = \"arrivalCityCoord\" />\n")
                .append("<input type = \"submit\" value = \"Submit\" />\n")
                .append("</form>\n")
                .append("</body>\n")
                .append("</html>\n")
                .toString();
    }
}
