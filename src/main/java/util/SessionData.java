package util;

import org.hibernate.cfg.Configuration;
import service.FlightService;
import service.UserService;

public class SessionData {

    public static String username;
    public static UserService userService= new UserService(new org.hibernate.cfg.Configuration().configure().buildSessionFactory());
    public static FlightService flightService = new FlightService(new Configuration().configure().buildSessionFactory());

}
