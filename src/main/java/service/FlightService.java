package service;

import dao.FlightDAO;
import model.Flight;
import org.hibernate.SessionFactory;

import java.util.List;

public class FlightService {

    private FlightDAO flightDAO;

    public FlightService(SessionFactory sessionFactory) {
        flightDAO = new FlightDAO(sessionFactory);
    }

    public Flight findFlightById(Integer id){
        return flightDAO.findFlight(id);
    }

    public List<Flight> findFlights(){
        return flightDAO.findFlights();
    }

    public Boolean delete(Integer id){
        return flightDAO.deleteFlight(id);
    }

    public Flight save(Flight flight){
        return flightDAO.addFlight(flight);
    }

    public Flight update(Flight flight){
        return flightDAO.updateFlight(flight);
    }
}
