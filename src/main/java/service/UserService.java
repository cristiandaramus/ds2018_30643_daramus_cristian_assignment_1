package service;

import dao.FlightDAO;
import dao.UserDAO;
import exceptions.LoginException;
import model.User;
import org.hibernate.SessionFactory;
import util.SessionData;

import java.util.ArrayList;
import java.util.List;

public class UserService {

    private UserDAO userDAO;
    private FlightDAO flightDAO;

    public UserService(SessionFactory sessionFactory) {
        userDAO = new UserDAO(sessionFactory);
        flightDAO = new FlightDAO(sessionFactory);
    }

    public String logIn(String username, String password) throws LoginException {

        User user = userDAO.findUser(username);
        if (user == null) {
            throw new LoginException("Not a user");
        } else {
            SessionData.username = user.getUsername();
            if (password.equals(user.getPassword())) {
                return user.getRole();
            } else {
                throw new LoginException("Not a user");
            }
        }
    }

    public String findUserRoleByUsername(String username) {

        User user = userDAO.findUser(username);
        return user.getRole();
    }

}
