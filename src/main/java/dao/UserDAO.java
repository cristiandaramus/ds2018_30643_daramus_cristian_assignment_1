package dao;


import model.User;
import org.hibernate.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author: Cristian Daramus
 * @Module: assignment-one-two-server
 * @Since: Oct 19, 2018
 * @Description: Uses Hibernate for CRUD operations on the underlying database.
 * The Hibernate configuration files can be found in the src/main/resources folder
 */

public class UserDAO {

    private SessionFactory factory;


    public UserDAO(SessionFactory factory) {
        this.factory = factory;
    }

    public User findUser(String username) {
        List<User> users = findUsersRows(username);
        return users != null && !users.isEmpty() ? users.get(0) : null;
    }

    public List<User> findUsersRows(String username) {
        Transaction tx = null;
        List<User> users = new ArrayList<>();
        Session session = factory.openSession();

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :username");
            query.setParameter("username", username);
            users = query.list();
            tx.commit();
        } catch (HibernateException e) {
            if (tx != null) {
                tx.rollback();
            }
        } finally {
            session.close();
        }
        return users;
    }
}
