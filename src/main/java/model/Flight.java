package model;

import java.sql.Timestamp;


public class Flight {

    private Integer id;
    private String airplaneType;
    private String departureCity;
    private Timestamp departureDateTime;
    private String departureCityLocalTime;
    private String arrivalCity;
    private Timestamp arrivalDateTime;
    private String arrivalCityLocalTime;

    public Flight() {
    }

    public Flight(String airplaneType, String departureCity, Timestamp departureDateTime, String departureCityLocalTime, String arrivalCity, Timestamp arrivalDateTime, String arrivalCityLocalTime) {
        this.airplaneType = airplaneType;
        this.departureCity = departureCity;
        this.departureDateTime = departureDateTime;
        this.departureCityLocalTime = departureCityLocalTime;
        this.arrivalCity = arrivalCity;
        this.arrivalDateTime = arrivalDateTime;
        this.arrivalCityLocalTime = arrivalCityLocalTime;
    }

    public Flight(Integer id, String airplaneType, String departureCity, Timestamp departureDateTime, String departureCityLocalTime, String arrivalCity, Timestamp arrivalDateTime, String arrivalCityLocalTime) {
        this.id = id;
        this.airplaneType = airplaneType;
        this.departureCity = departureCity;
        this.departureDateTime = departureDateTime;
        this.departureCityLocalTime = departureCityLocalTime;
        this.arrivalCity = arrivalCity;
        this.arrivalDateTime = arrivalDateTime;
        this.arrivalCityLocalTime = arrivalCityLocalTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public Timestamp getDepartureDateTime() {
        return departureDateTime;
    }

    public void setDepartureDateTime(Timestamp departureDateTime) {
        this.departureDateTime = departureDateTime;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Timestamp getArrivalDateTime() {
        return arrivalDateTime;
    }

    public void setArrivalDateTime(Timestamp arrivalDateTime) {
        this.arrivalDateTime = arrivalDateTime;
    }

    public String getDepartureCityLocalTime() {
        return departureCityLocalTime;
    }

    public void setDepartureCityLocalTime(String departureCityLocalTime) {
        this.departureCityLocalTime = departureCityLocalTime;
    }

    public String getArrivalCityLocalTime() {
        return arrivalCityLocalTime;
    }

    public void setArrivalCityLocalTime(String arrivalCityLocalTime) {
        this.arrivalCityLocalTime = arrivalCityLocalTime;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", airplaneType='" + airplaneType + '\'' +
                ", departureCity='" + departureCity + '\'' +
                ", departureDateTime=" + departureDateTime +
                ", arrivalCity='" + arrivalCity + '\'' +
                ", arrivalDateTime=" + arrivalDateTime +
                '}';
    }
}
